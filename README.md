# Felixent Loan

## What is amortization of a loan?

Amortization is paying off a debt over time in equal installments. Part of each payment goes toward the loan principal, and part goes toward interest. As the loan amortizes, the amount going toward principal starts out small, and gradually grows larger month by month.

## Paying Off a Loan Over Time

When a borrower takes out a mortgage, car loan, or personal loan, they usually make monthly payments to the lender; these are some of the most common uses of amortization. A part of the payment covers the interest due on the loan, and the remainder of the payment goes toward reducing the principal amount owed. Interest is computed on the current amount owed and thus will become progressively smaller as the principal decreases. It is possible to see this in action on the amortization table.

## Amortization Schedule

An amortization schedule (sometimes called an amortization table) is a table detailing each periodic payment on an amortizing loan. Each calculation done by the calculator will also come with an annual and monthly amortization schedule above. Each repayment for an amortized loan will contain both an interest payment and payment towards the principal balance, which varies for each pay period. An amortization schedule helps indicate the specific amount that will be paid towards each, along with the interest and principal paid to date, and the remaining principal balance after each pay period.
Basic amortization schedules do not account for extra payments, but this doesn't mean that borrowers can't pay extra towards their loans. Also, amortization schedules generally do not consider fees. Generally, amortization schedules only work for fixed-rate loans and not adjustable-rate mortgages, variable rate loans, or lines of credit.

## Spreading Costs

Certain businesses sometimes purchase expensive items that are used for long periods of time that are classified as investments. Items that are commonly amortized for the purpose of spreading costs include machinery, buildings, and equipment. From an accounting perspective, a sudden purchase of an expensive factory during a quarterly period can skew the financials, so its value is amortized over the expected life of the factory instead. Although it can technically be considered amortizing, this is usually referred to as the depreciation expense of an asset amortized over its expected lifetime. For more information about or to do calculations involving depreciation, please visit the [Amortization Calculator.](https://gitlab.com/sethuaung/felixent-loan/)

Amortization as a way of spreading business costs in accounting generally refers to intangible assets like a patent or copyright. The value of these assets can be deducted month-to-month or year-to-year. Just like with any other amortization, payment schedules can be forecasted by a calculated amortization schedule. The following are intangible assets that are often amortized:

1. Goodwill, which is the reputation of a business regarded as a quantifiable asset
2. Going-concern value, which is the value of a business as an ongoing entity
3. The workforce in place (current employees, including their experience, education, and training)
4. Business books and records, operating systems, or any other information base, including lists or other information concerning current or prospective customers
5. Patents, copyrights, formulas, processes, designs, patterns, know-hows, formats, or similar items
6. Customer-based intangibles, including customer bases and relationships with customers
7. Supplier-based intangibles, including the value of future purchases due to existing relationships with vendors
8. Licenses, permits, or other rights granted by governmental units or agencies (including issuances and renewals)
9. Covenants not to compete or non-compete agreements entered relating to acquisitions of interests in trades or businesses
10. Franchises, trademarks, or trade names
11. Contracts for the use of or term interests in any items on this list


## Privacy Policy
[Felixent Technical Service Limited](https://sites.google.com/view/felixent/home) operates the many webservices, which provides the SERVICE.

This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.

If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.

## Security

We value your trust in providing us your Personal Information. Thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the Internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

## Links to Other Sites

Our Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Conduct

You agree not to post any text, files, images, video, audio, or other materials ("Content") or use the Website in any way that:

infringes any patent, trademark, trade secret, copyright, or other proprietary rights of any party;
violates any state, federal, or other law;
threatens, harasses, or is libelous;
is harmful to minors;
contains self-benefiting advertising or marketing in public areas of the Website that have not been paid for and are not designated for the addition of promotional content;
produces Software viruses or code harmful to other computers;
disrupts the normal dialogue of users of the Website;
employs misleading or false information;
uses forged headers or other items to manipulate identifiers in order to disguise the origin of Content.
You agree not to decompile or reverse engineer or otherwise attempt to discover any source code contained in the Website.

Unless you receive explicit permission, you agree not to reproduce, duplicate, copy, sell, resell, or exploit for any commercial purposes, any aspect of the Website. Any business that registers with a listing or purchases advertising or any other service ("Customer") has the right and permission to manage their listing or purchased advertising or other services to their commercial benefit so long as the codes of conduct above are not violated.

## Disclaimer of Warranties

THE COMPANY WEBSITE AND ANY INCLUDED SERVICES ARE PROVIDED ON AN "AS IS" BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT IS FREE OF DEFECTS, ERRORS, VIRUSES, MERCHANTABLE AND THAT IT IS FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. YOUR USE OF THE WEBSITE IS AT YOUR OWN RISK.

## Limitations of Liability

UNDER NO CIRCUMSTANCES SHALL THE COMPANY BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES, RESULTING FROM ANY ASPECT OF YOUR USE OF THE WEBSITE INCLUDING BUT NOT LIMITED TO DAMAGES THAT ARISE FROM YOUR INABILITY TO USE THE WEBSITE OR THE SERVICE, OR THE INTERRUPTION, MODIFICATION, OR TERMINATION OF THE WEBSITE OR ANY SERVICE OR PART THEREOF.

## Jurisdiction

The Terms of Use shall be governed by the laws of the State of Rangoon without regard to its conflict of law provisions. You and Company agree to submit to the personal and exclusive jurisdiction of the courts located within the State of Rangoon.

## License
Open source projects.
